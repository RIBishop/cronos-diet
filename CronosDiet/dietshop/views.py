from django.core.paginator import Paginator
from django.http import JsonResponse
from django.shortcuts import render
from django.views.generic import ListView

from .models import *
import datetime
import json


# Create your views here.
def index(request):
    context = {}
    return render(request, template_name='index.html', context=context)


def shop(request):
    if request.user.is_authenticated:
        customer, created = Customer.objects.get_or_create()
        order, created = Order.objects.get_or_create(customer=customer, complete=False)
        items = order.orderitem_set.all()
        basketItems = order.get_basket_items
    else:
        items = []
        order = {'get_basket_total': 0, 'get_basket_items': 0}
        basketItems = order['get_basket_items']

    products = Product.objects.all()
    #paginator = Paginator(products, 4)
    #page_number = request.GET.get('page')
    #page_obj = paginator.get_page(page_number)
    context = {'products': products, 'basketItems': basketItems, 'items': items} #'products': page_obj

    return render(request, template_name='shop.html', context=context)


def product_details(request, pk):
    if request.user.is_authenticated:
        customer, created = Customer.objects.get_or_create()
        order, created = Order.objects.get_or_create(customer=customer, complete=False)
        items = order.orderitem_set.all()
        basketItems = order.get_basket_items
    else:
        items = []
        order = {'get_basket_total': 0, 'get_basket_items': 0}
        basketItems = order['get_basket_items']

    product = Product.objects.get(pk=pk)
    context = {'product': product, 'items': items, 'basketItems': basketItems, 'order': order}
    return render(request, template_name='product_details.html', context=context)


def basket(request):
    if request.user.is_authenticated:
        customer, created = Customer.objects.get_or_create()
        order, created = Order.objects.get_or_create(customer=customer, complete=False)
        items = order.orderitem_set.all()
        basketItems = order.get_basket_items

    else:
        items = []
        order = {'get_basket_total': 0, 'get_basket_items': 0}
        basketItems = order['get_basket_items']

    context = {'items': items, 'order': order, 'basketItems': basketItems}
    return render(request, template_name='basket.html', context=context)


def checkout(request):
    if request.user.is_authenticated:
        customer, created = Customer.objects.get_or_create()
        order, created = Order.objects.get_or_create(customer=customer, complete=False)
        items = order.orderitem_set.all()
        basketItems = order.get_basket_items

    else:
        items = []
        order = {'get_basket_total': 0, 'get_basket_items': 0}
        basketItems = order['get_basket_items']

    context = {'items': items, 'order': order, 'basketItems': basketItems}
    return render(request, template_name='checkout.html', context=context)


def updateItem(request):
    data = json.loads(request.body)
    productId = data['productId']
    action = data['action']

    print('Action:', action)
    print('ProductId:', productId)

    customer, created = Customer.objects.get_or_create()
    product = Product.objects.get(id=productId)
    order, created = Order.objects.get_or_create(customer=customer, complete=False)

    order_item, created = OrderItem.objects.get_or_create(order=order, product=product)

    if action == 'add':
        order_item.quantity = (order_item.quantity + 1)
    elif action == 'remove':
        order_item.quantity = (order_item.quantity - 1)
    order_item.save()

    if order_item.quantity <= 0:
        order_item.delete()

    return JsonResponse('Item was added', safe=False)


def processOrder(request):
    transaction_id = datetime.datetime.now().timestamp()
    data = json.loads(request.body)
    if request.user.is_authenticated:
        customer, created = Customer.objects.get_or_create()
        order, created = Order.objects.get_or_create(customer=customer, complete=False)
        total = float(data['form']['total'])
        order.transaction_id = transaction_id

        if total == order.get_basket_total:
            order.complete = True
        order.save()

        ShippingAddress.objects.create(
            customer=customer,
            order=order,
            address=data['shipping']['address'],
            city=data['shipping']['city'],
            zipcode=data['shipping']['zipcode'],
            country=data['shipping']['country'],
        )

    else:
        print('User is not logged in')
    return JsonResponse('Payment complete!', safe=False)


def body_analysis(request):
    context = {}
    return render(request, template_name='body_analysis.html', context=context)



