from django.apps import AppConfig


class DietshopConfig(AppConfig):
    name = 'dietshop'
