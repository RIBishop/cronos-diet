import json

import requests
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from .forms import UserCreateForm
from django.contrib import messages

# Create your views here.
def user_register(request):
    form = UserCreateForm()

    if request.method == 'POST':
        form = UserCreateForm(request.POST)
        if form.is_valid():
            form.save()
            user = form.cleaned_data.get('username')
            messages.success(request, 'Account was created for ' + user)
            return redirect('login')
        else:
            messages.info(request, "The 2 passwords didn't match!")

    context = {'form': form}
    return render(request, template_name='register.html', context=context)


def user_login(request):

    if request.method == 'POST':
        captcha_token = request.POST.get("g-recaptcha-response")
        captcha_url = "https://www.google.com/recaptcha/api/siteverify"
        captcha_secret_key = "6Lf27lQaAAAAAE-iM3F6-fbmvbVZ0lXlKQKf6Svz"
        captcha_data = {'secret': captcha_secret_key, 'response': captcha_token}
        captcha_server_response = requests.post(url=captcha_url, data=captcha_data)
        captcha_json = json.loads(captcha_server_response.text)
        if captcha_json['success']==False:
            messages.info(request, 'Invalid Captcha! Try againg!')
            return redirect('login')

        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('shop')
        else:
            messages.info(request, 'Username/password is incorrect!')

    context = {}
    return render(request, template_name='login.html', context=context)


def user_logout(request):
    logout(request)
    return redirect('login')


# Create your views here.
